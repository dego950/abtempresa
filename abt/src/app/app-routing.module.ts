import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ServiciosComponent} from './servicios/servicios.component';
import {AboutComponent} from './about/about.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path : '', redirectTo: '/home', pathMatch : 'full'},
  { path: 'servicios', component: ServiciosComponent},
  { path: 'about', component: AboutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
