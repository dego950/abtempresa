import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  activetab = '';

  constructor() { }

  ngOnInit() {
  }

  getActiveTab(tabname: string) {
   this.activetab = tabname;
  }

  refresh(): void {
    window.location.reload();
  }

}
